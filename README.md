# mykro/jetbrains-upsource
A build on top of Debian 8
For information on Upsource check out: https://www.jetbrains.com/upsource/

## Image Sizes
[![](https://badge.imagelayers.io/mykro/jetbrains-upsource:latest.svg)](https://imagelayers.io/?images=mykro/jetbrains-upsource:latest) 2.5.4995, latest

## Docker Switches
### -p --port
This image runs on port 8080 and assumes being linked to a loadbalancer or reverse proxy such as Nginx. To ensure porting out the correct port use -p HOST_PORT:DOCKER_PORT.
```sh
$ docker run -d -p 10080:8080 mykro/jetbrains-upsource
```

### -v --volumes
To enable data persistence, make sure all folders created have been chown'ed to docker:docker.

#### Root home folder
```sh
$ docker run -d -v /HOST/FOLDER/:/jetbrains/upsource/ mykro/jetbrains-upsource
```

#### Backup folder mapping
```sh
$ docker run -d -v /HOST/FOLDER/backup:/jetbrains/upsource/backup mykro/jetbrains-upsource
```

#### Data Folder
```sh
$ docker run -d -v /HOST/FOLDER/data:/jetbrains/upsource/data mykro/jetbrains-upsource
```

#### Log Folder: 
```sh
$ docker run -d -v /HOST/FOLDER/logs:/jetbrains/upsource/logs mykro/jetbrains-upsource
```

#### Temp Folder 
```sh
$ docker run -d -v /jetbrains/upsource/tmp:/jetbrains/upsource/tmp mykro/jetbrains-upsource
```

### --name
This is a nice switch which allows you to name an instance. This is handy if you're running High Availability with multiple containers mapped to the same host folders. This way it's easier to define the different containers under docker stats, and start/stop commands. 
For example:
```sh
$ docker run -d --name=jbus-001 -p 8081:8080 mykro/jetbrains-upsource
$ docker run -d --name=jbus-002 -p 8082:8080 mykro/jetbrains-upsource
$ docker stop jbus-001
$ docker start jbus-001
$ docker stats jbus-001 jbus-002
```

### --restart
If in event of container failure, you can get docker to auto-restart by using --restart=always
For example:
```sh
$ docker run -d --restart=always mykro/jetbrains-upsource
```

## Running Images
So, putting it all together you'll end up with a command as follows. As mentioned above, ensure that docker has permissions on the host folders otherwise it will fail.
```sh
$ docker run -d \
   -p 10301:8080 \
   -v /HOST/FOLDER/:/jetbrains/upsource/ \
   -v /HOST/FOLDER/backup:/jetbrains/upsource/backup \
   -v /HOST/FOLDER/data:/jetbrains/upsource/data \
   -v /HOST/FOLDER/logs:/jetbrains/upsource/logs \
   -v /HOST/FOLDER/tmp:/jetbrains/upsource/tmp \
   --name=jbyt-001 \
   --restart=always \
   mykro/jetbrains-upsource
```

## Building a custom image
This image depends on the Server runtime version of Java I have built in mykro/java8-jre. This can be pulled with
```sh
$ docker pull mykro/java8-jre:latest
```
Make your edits under the build file, namely the location of files within the container. I have added an Environment Variable that you can change to specify how much RAM you are allowing Java to consume when running. Currently, it is set in the build file as 1024MBs.

## Pulling different versions
Pull a different image with: 
```sh
$ docker pull mykro/jetbrains-upsource:[latest|2.5.4995]
```
To see more versions, check the image tag tab above!

## Bugs/Issues/Updates
If you've found a bug/issue, you can find the repo @ https://bitbucket.org/mykro/jetbrains-upsource/overview