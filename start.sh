#!/usr/bin/env bash
docker run -d \
   -p 10301:8080 \
   -v /jetbrains/upsource/:/jetbrains/upsource/ \
   -v /jetbrains/upsource/backup:/jetbrains/upsource/backup \
   -v /jetbrains/upsource/data:/jetbrains/upsource/data \
   -v /jetbrains/upsource/logs:/jetbrains/upsource/logs \
   -v /jetbrains/upsource/tmp:/jetbrains/upsource/tmp \
   --name=jb-yt-001 \
   --restart=always \
   mykro/jetbrains-upsource:latest
